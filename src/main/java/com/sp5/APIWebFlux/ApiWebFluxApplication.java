package com.sp5.APIWebFlux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiWebFluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiWebFluxApplication.class, args);
	}

}
