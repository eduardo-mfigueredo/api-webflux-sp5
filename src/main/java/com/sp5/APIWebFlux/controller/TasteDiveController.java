package com.sp5.APIWebFlux.controller;

import com.sp5.APIWebFlux.integration.webclient.TasteDiveClient;
import com.sp5.APIWebFlux.models.tastedive.TasteDiveEntity;
import com.sp5.APIWebFlux.models.tastedive.TasteDiveRequest;
import com.sp5.APIWebFlux.services.TasteDiveService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
@RequestMapping("/v1/search-similars")
public class TasteDiveController {

    private final TasteDiveService tasteDiveService;

    @GetMapping()
    public Mono<TasteDiveEntity> tasteDiveSearch(@RequestParam TasteDiveRequest search) {
        return tasteDiveService.searchSimilars(search);
    }

}
