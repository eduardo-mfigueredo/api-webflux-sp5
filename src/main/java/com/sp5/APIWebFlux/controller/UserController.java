package com.sp5.APIWebFlux.controller;

import com.sp5.APIWebFlux.facade.UserFacade;
import com.sp5.APIWebFlux.models.FilterRequest;
import com.sp5.APIWebFlux.models.UserRequest;
import com.sp5.APIWebFlux.models.UserResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("v1/users")
public class UserController {

    private final UserFacade userFacade;

    @GetMapping
    public Flux<UserResponse> getAllUsers() {
        return userFacade.allUsers();
    }

    @GetMapping("/find")
    public Mono<UserResponse> getUserById(@RequestParam String id) {
        return userFacade.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<UserResponse> createNewUser(@RequestBody UserRequest userRequest) {
        return userFacade.createUser(userRequest);
    }

    @PutMapping()
    @ResponseStatus(HttpStatus.ACCEPTED)
    Mono<UserResponse> updateUser(@RequestBody UserRequest userRequest, @RequestParam String id) {
        return userFacade.updateUser(userRequest, id);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    Mono<Void> deleteUsers(@RequestParam List<String> ids) {
        return userFacade.deleteUsers(ids);
    }
}
