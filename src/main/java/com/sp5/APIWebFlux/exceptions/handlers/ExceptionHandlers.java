package com.sp5.APIWebFlux.exceptions.handlers;

import com.sp5.APIWebFlux.exceptions.notfound.NotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ServerWebInputException;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.List;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;

@RestControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public Mono<StandardError> notFound(NotFoundException e) {
        return Mono.just((StandardError.builder()
                .timestamp(Instant.now())
                .errorObject(List.of(StandardErrorObject.builder()
                        .status(NOT_FOUND.value())
                        .errorMessage("Não encontrado.")
                        .message(e.getMessage())
                        .build()))
                .build()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public Mono<StandardError> badRequestError(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrorList = e.getBindingResult().getFieldErrors();
        return Mono.just((StandardError.builder()
                .timestamp(Instant.now())
                .errorObject(fieldErrorList.stream().map(fieldError -> StandardErrorObject.builder()
                        .errorMessage("Requisição não foi feita corretamente. Verifique!")
                        .message(fieldError.getField().concat(fieldError.getDefaultMessage()))
                        .status(BAD_REQUEST.value())
                        .build()).toList())
                .build()));
    }

    @ExceptionHandler(ServerWebInputException.class)
    @ResponseStatus(METHOD_NOT_ALLOWED)
    public StandardError methodNotAllowedError(ServerWebInputException e) {
        return StandardError.builder()
                .timestamp(Instant.now())
                .errorObject(List.of(StandardErrorObject.builder()
                        .status(METHOD_NOT_ALLOWED.value())
                        .errorMessage("O método chamado não pode ser requisitado nessa url. Verifique!")
                        .message(e.getMessage())
                        .build()))
                .build();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(BAD_REQUEST)
    public StandardError illegalArgumentError (IllegalArgumentException e) {
        return StandardError.builder()
                .timestamp(Instant.now())
                .errorObject(List.of(StandardErrorObject.builder()
                        .status(BAD_REQUEST.value())
                        .errorMessage("Argumentos inválidos, confira sua requisição.")
                        .message(e.getMessage())
                        .build()))
                .build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public StandardError defaultError(Exception e) {
        return StandardError.builder()
                .timestamp(Instant.now())
                .errorObject(List.of(StandardErrorObject.builder()
                        .errorMessage("Deu ruim!")
                        .message(e.getClass().toString())
                        .build()))
                .build();
    }

}
