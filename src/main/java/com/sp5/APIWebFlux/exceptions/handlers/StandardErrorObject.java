package com.sp5.APIWebFlux.exceptions.handlers;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class StandardErrorObject {

    private Integer status;
    private String errorMessage;
    private String message;

}
