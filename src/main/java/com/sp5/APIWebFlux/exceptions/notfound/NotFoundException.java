package com.sp5.APIWebFlux.exceptions.notfound;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String msg) {
        super(msg);
    }
}
