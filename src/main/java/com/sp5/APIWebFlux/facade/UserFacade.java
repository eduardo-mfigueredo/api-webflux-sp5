package com.sp5.APIWebFlux.facade;

import com.sp5.APIWebFlux.exceptions.notfound.NotFoundException;
import com.sp5.APIWebFlux.integration.webclient.EmailValidationClient;
import com.sp5.APIWebFlux.models.FilterRequest;
import com.sp5.APIWebFlux.models.UserRequest;
import com.sp5.APIWebFlux.models.UserResponse;
import com.sp5.APIWebFlux.models.mappers.UserRequestMapper;
import com.sp5.APIWebFlux.models.mappers.UserResponseMapper;
import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import com.sp5.APIWebFlux.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@AllArgsConstructor
@Component
public class UserFacade {

    private final UserService userService;
    private final EmailValidationClient emailValidationClient;

    public Flux<UserResponse> allUsers() {
        return userService.allUsers().map(UserResponseMapper.INSTANCE::userEntityToResponse);
    }

    public Mono<UserResponse> findById(String id) {
        return userService.findById(id).map(UserResponseMapper.INSTANCE::userEntityToResponse);
    }

    public Mono<UserResponse> createUser(UserRequest userRequest) {
        return Mono.just(UserRequestMapper.INSTANCE.userRequestToEntity(userRequest))
                .flatMap(userEntity -> {
                    return emailValidationClient.validation(userRequest.getEmail()).flatMap(emailValidator -> {
                        userEntity.setEmail(emailValidator);
                        return Mono.just(userEntity);
                    });
                })
                .flatMap(userService::createUser)
                .map(UserResponseMapper.INSTANCE::userEntityToResponse);
    }

    public Mono<UserResponse> updateUser(UserRequest userRequest, String id) {
        return Mono.just(UserRequestMapper.INSTANCE.userRequestToEntity(userRequest))
                .flatMap(userEntity -> {
                    return emailValidationClient.validation(userRequest.getEmail()).flatMap(emailValidator -> {
                        userEntity.setEmail(emailValidator);
                        return Mono.just(userEntity);
                    });
                })
                .flatMap(userEntity -> userService.updateUser(userEntity, id))
                .map(UserResponseMapper.INSTANCE::userEntityToResponse);
    }

    public Mono<Void> deleteUsers(List<String> ids) {
        return userService.deleteUsers(ids);
    }

}
