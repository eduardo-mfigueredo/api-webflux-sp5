package com.sp5.APIWebFlux.integration.webclient;

import com.sp5.APIWebFlux.integration.EmailValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class EmailValidationClient {

    private final WebClient webClient;

    public EmailValidationClient(WebClient.Builder webClientBuilder){
        this.webClient = webClientBuilder.baseUrl("https://emailvalidation.abstractapi.com").build();
    }

    public Mono<EmailValidator> validation(String emailToValidate) {
        return this.webClient.get().uri("/v1/?api_key=5280fc333bd84e0baf73998e8b6cb325&email=" + emailToValidate)
                .retrieve().bodyToMono(EmailValidator.class);
    }
}
