package com.sp5.APIWebFlux.integration.webclient;

import com.sp5.APIWebFlux.models.tastedive.TasteDiveEntity;
import com.sp5.APIWebFlux.models.tastedive.TasteDiveRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class TasteDiveClient {

    private final WebClient webClient2;

    public TasteDiveClient(WebClient.Builder webClientBuilder){
        this.webClient2 = webClientBuilder.baseUrl("https://tastedive.com").build();
    }

    public Mono<TasteDiveEntity> tasteDiveSearch(TasteDiveRequest tasteDiveRequest) {
        return this.webClient2.get().uri("/api/similar?k=440807-Testing-9GCMDYC9&verbose=1&q=" +
                        tasteDiveRequest.getSearch()).retrieve().bodyToMono(TasteDiveEntity.class);
    }

}
