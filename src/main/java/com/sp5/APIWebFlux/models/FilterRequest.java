package com.sp5.APIWebFlux.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FilterRequest {

    private String id;
    private String deliverability;
    private String emailDomain;

}
