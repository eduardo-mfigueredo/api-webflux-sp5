package com.sp5.APIWebFlux.models;

import com.sp5.APIWebFlux.integration.EmailValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserRequest {

    @NotBlank(message = " não pode estar em branco.")
    private String name;

    @NotBlank(message = " não pode estar em branco.")
    @Size(min = 8, message = "Mínimo 8 caracteres")
    @Email(message = "Email inválido")
    private String email;

}
