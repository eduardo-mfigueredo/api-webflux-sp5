package com.sp5.APIWebFlux.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sp5.APIWebFlux.integration.EmailValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserResponse {

    private String id;
    private String name;
    private EmailValidator email;
    @JsonFormat(pattern = "dd-MM-yyyy, HH:mm")
    private LocalDateTime registrationDate;

}
