package com.sp5.APIWebFlux.models.mappers;

import com.sp5.APIWebFlux.integration.EmailValidator;
import com.sp5.APIWebFlux.models.UserRequest;
import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;

@Mapper()
public interface UserRequestMapper {


    UserRequestMapper INSTANCE = Mappers.getMapper(UserRequestMapper.class);


    default UserEntity userRequestToEntity(UserRequest userRequest) {
        return UserEntity.builder()
                .name(userRequest.getName())
                .email(EmailValidator.builder()
                        .email(userRequest.getEmail())
                        .build())
                .registrationDate(LocalDateTime.now())
                .build();
    }

//    @AfterMapping
//    default void setRegistrationDate(@MappingTarget UserEntity.UserEntityBuilder userEntity){
//        userEntity.registrationDate(LocalDateTime.now()).build();
//    }
}
