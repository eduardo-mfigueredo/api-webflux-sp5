package com.sp5.APIWebFlux.models.mappers;

import com.sp5.APIWebFlux.integration.EmailValidator;
import com.sp5.APIWebFlux.models.UserResponse;
import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import lombok.Builder;
import lombok.experimental.UtilityClass;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserResponseMapper {

    UserResponseMapper INSTANCE = Mappers.getMapper(UserResponseMapper.class);

    UserResponse userEntityToResponse(UserEntity userEntity);


}
