package com.sp5.APIWebFlux.models.tastedive;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Info {

    @JsonProperty("Name")
    private String name;
    @JsonProperty("Type")
    private String type;

}
