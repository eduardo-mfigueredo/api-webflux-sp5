package com.sp5.APIWebFlux.models.tastedive;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Similar {

    @JsonProperty("Info")
    @Builder.Default
    protected ArrayList<Info> info = new ArrayList<>();
    @JsonProperty("Results")
    @Builder.Default
    protected ArrayList<Result> results = new ArrayList<>();

}
