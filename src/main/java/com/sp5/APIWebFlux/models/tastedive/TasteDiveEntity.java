package com.sp5.APIWebFlux.models.tastedive;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Document(collection = "TasteDiveQueries")
public class TasteDiveEntity {

    @JsonProperty("Similar")
    private Similar similar;

}
