package com.sp5.APIWebFlux.models.tastedive;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class TasteDiveRequest {

    @NotNull
    private String search;

}
