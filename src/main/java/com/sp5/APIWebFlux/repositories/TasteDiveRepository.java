package com.sp5.APIWebFlux.repositories;

import com.sp5.APIWebFlux.models.tastedive.TasteDiveEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TasteDiveRepository extends ReactiveMongoRepository<TasteDiveEntity, String> {
}
