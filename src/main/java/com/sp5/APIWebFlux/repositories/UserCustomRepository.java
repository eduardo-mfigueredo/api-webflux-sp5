package com.sp5.APIWebFlux.repositories;

import com.sp5.APIWebFlux.models.FilterRequest;
import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import org.springframework.data.domain.PageRequest;
import reactor.core.publisher.Flux;

import java.util.List;

public interface UserCustomRepository {

    Flux<UserEntity> sortUsersByProperties(FilterRequest filterRequest, PageRequest pageRequest);

}
