package com.sp5.APIWebFlux.repositories;

import com.sp5.APIWebFlux.models.FilterRequest;
import com.sp5.APIWebFlux.repositories.UserCustomRepository;
import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {

    private final ReactiveMongoTemplate reactiveMongoTemplate;

    @Override
    public Flux<UserEntity> sortUsersByProperties(FilterRequest filterRequest, PageRequest pageRequest) {
        Query query = new Query().with(pageRequest);

        final List<Criteria> criteria = new ArrayList<>();

        if (filterRequest.getId() != null && !filterRequest.getId().isEmpty())
            criteria.add(Criteria.where("id").is(filterRequest.getId()));

        if (filterRequest.getDeliverability() != null && !filterRequest.getDeliverability().isEmpty())
            criteria.add(Criteria.where("email.deliverability").is(filterRequest.getDeliverability().toUpperCase()));

        if (filterRequest.getEmailDomain() != null && !filterRequest.getEmailDomain().isEmpty())
            criteria.add(Criteria.where("email.email").regex(filterRequest.getEmailDomain()));

        query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[0])));

        return reactiveMongoTemplate.find(query, UserEntity.class);
    }
}
