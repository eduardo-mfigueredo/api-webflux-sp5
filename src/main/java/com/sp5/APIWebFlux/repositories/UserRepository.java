package com.sp5.APIWebFlux.repositories;

import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends ReactiveMongoRepository<UserEntity, String>, UserCustomRepository {
}
