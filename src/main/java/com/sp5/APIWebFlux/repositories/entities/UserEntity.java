package com.sp5.APIWebFlux.repositories.entities;


import com.sp5.APIWebFlux.integration.EmailValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document("Users")
public class UserEntity {

    @Id
    private String id;
    private String name;
    private EmailValidator email;
    private LocalDateTime registrationDate;

}
