package com.sp5.APIWebFlux.services;

import com.sp5.APIWebFlux.exceptions.notfound.NotFoundException;
import com.sp5.APIWebFlux.integration.webclient.TasteDiveClient;
import com.sp5.APIWebFlux.models.tastedive.TasteDiveEntity;
import com.sp5.APIWebFlux.models.tastedive.TasteDiveRequest;
import com.sp5.APIWebFlux.repositories.TasteDiveRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Optional;

@AllArgsConstructor
@Service
public class TasteDiveService {

    private final TasteDiveRepository tasteDiveRepository;
    private final TasteDiveClient tasteDiveClient;

    public Mono<TasteDiveEntity> searchSimilars(TasteDiveRequest tasteDiveRequest) {
//        TasteDiveEntity tasteDiveEntity = Optional.ofNullable(tasteDiveRequest)
//                .map((TasteDiveRequest tasteDiveRequest1) -> tasteDiveClient.tasteDiveSearch(tasteDiveRequest))
//                .map((tasteDiveRepository::save))
//                .orElseThrow(() -> new NotFoundException("Impossível resolver a consulta"));
//
//        if (tasteDiveEntity.getSimilar().getResults().isEmpty()) {
//            throw new NotFoundException("Busca sem resultados.");
//        }
//        return tasteDiveEntity;

        return tasteDiveClient.tasteDiveSearch(tasteDiveRequest)
                .flatMap(tasteDiveRepository::save)
                .switchIfEmpty(Mono.error(new NotFoundException("Impossível resolver a consulta.")));

    }

}
