package com.sp5.APIWebFlux.services;

import com.sp5.APIWebFlux.exceptions.notfound.NotFoundException;
import com.sp5.APIWebFlux.repositories.UserRepository;
import com.sp5.APIWebFlux.repositories.entities.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


@Service
@AllArgsConstructor
public class UserService {

    public final UserRepository userRepository;

    public Flux<UserEntity> allUsers() {
        return userRepository.findAll();
    }

    public Mono<UserEntity> findById(String id) {
        return userRepository.findById(id).switchIfEmpty(Mono.error(new NotFoundException("Id não encontrado")));
    }

    public Mono<UserEntity> createUser(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    public Mono<UserEntity> updateUser(UserEntity userEntity, String id) {
        return userRepository.findById(id).map(userEntityInDB -> {
                    userEntity.setId(userEntityInDB.getId());
                    return userEntity;
                }).flatMap(userRepository::save)
                .switchIfEmpty(Mono.error(new NotFoundException("Id não encontrado")));
    }

    public Mono<Void> deleteUsers(List<String> ids) {
        return userRepository.deleteAllById(ids);
    }
}



